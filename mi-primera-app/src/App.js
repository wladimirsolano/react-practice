import React, {useEffect, useState} from 'react';
import logo from './logo.svg';
import './App.css';

function App() {

    const [stateCar, setStateCar] = useState(false);
    const [stateCount, setStateCount] = useState(0);

    useEffect( () => {
         console.log("Count", stateCount)
    }, [stateCount]);

    const changeState = () => {
        setStateCar(!stateCar);
        setStateCount(stateCount + 1)
    }

    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h3>The Car is {stateCar ? "on" : "off"}</h3>
                <h4>Clicks: {stateCount}</h4>
                <button onClick={changeState}>{stateCar ? "Turn off" : "Turn on"}</button>
            </header>
        </div>
    );
}

export default App;
