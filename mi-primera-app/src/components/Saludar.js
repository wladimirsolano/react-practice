import React from "react";

function Saludar(props) {

    const { infoUsuario, saludarUsuario} = props;
    const { nombre = "Anonimo", edad} = infoUsuario

    console.log(props, infoUsuario, saludarUsuario);
    return (
        <div>
            <button onClick={() => saludarUsuario(nombre, edad)}>
                Saludar
            </button>
        </div>
    )
}

export default Saludar;